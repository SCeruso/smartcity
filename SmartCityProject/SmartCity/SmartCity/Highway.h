#pragma once
#include"Node.h"
#include <string>

enum HighWayType { MOTORWAY, PRIMARY, SECONDARY, TERTIARY, RESIDENTIAL, PEDESTRIAN, STEP, FOOTWAY, SECONDARY_LINK, STEPS,SERVICE };

using namespace std;

class Highway
{
private:
	Node* origin;
	double distance;
	int maxSpeed;
	HighWayType type;
	Node* destiny;

public:
	Highway(Node*, double, string, Node*);
	Highway(Node*, double,int ,string, Node*);
	~Highway();

	bool hasSpeedLimit();

private:
	HighWayType getType(string);
};

