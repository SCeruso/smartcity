#pragma once

#include "../lib/pugixml-1.7/src/pugixml.hpp"
#include "OSMap.h"
#include <iostream>

class OSMParser
{
private:
	OSMap* map;
	string filename;
public:
	OSMParser(string);
	OSMap* parse();
	OSMap* getMap();
	~OSMParser();

private:
	void parseHighWay(pugi::xml_node*, pugi::xml_node*);
	void createHighWay(vector<unsigned long long>&, string type, int way, int maxspeed);
};

