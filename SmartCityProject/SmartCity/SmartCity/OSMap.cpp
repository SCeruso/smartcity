#include "OSMap.h"



OSMap::OSMap()
{
	isNormalized = false;
}

OSMap::~OSMap()
{
	// Borrar primero todos los highway.
	// Borrar todos los nodos.
}

Node * OSMap::addNode(unsigned long long id, double lat, double lon)
{
	
	if (nodes.find(id) == nodes.end()) {
		nodes.insert({ id, new Node(id, lat, lon) });
		graph.insert({id, vector<Highway*>()});
	}

	return nodes.at(id);
}

Node * OSMap::getNode(unsigned long long id)
{
	return nodes.at(id);
}

bool OSMap::hasNode(unsigned long long id)
{
	return nodes.find(id) == nodes.end() ? false : true;
}

void OSMap::addHighway(Node *origin, string type, Node *destiny, int maxspeed)
{
	graph.at(origin->getId()).push_back(new Highway(origin, Node::calculateDistance(*origin, *destiny), maxspeed, type, destiny));
}

void OSMap::addHighway(Node *origin, string type, Node *destiny)
{
	graph.at(origin->getId()).push_back(new Highway(origin, Node::calculateDistance(*origin, *destiny), type, destiny));
}

void OSMap::setBounds(double minLat, double minLon, double maxLat, double maxLon)
{
	bounds.minLat = minLat;
	bounds.minLon = minLon;
	bounds.maxLon = maxLon;
	bounds.maxLat = maxLat;
}

void OSMap::setUTMBounds(double minX, double minY, double maxX, double maxY)
{
	utmBounds.minX = minX;
	utmBounds.minY = minY;
	utmBounds.maxX = maxX;
	utmBounds.maxY = maxY;
}

void OSMap::normalize()
{
	vector<unsigned long long> keys;

	if (isNormalized)
		return;

	for (auto it : nodes) 
		keys.push_back(it.first);
	
	for (int i = 0; i < keys.size(); i++) {
		nodes[keys[i]]->setCoordX(nodes[keys[i]]->getCoordX() - getUTMBounds().minX);
		nodes[keys[i]]->setCoordY(nodes[keys[i]]->getCoordY() - getUTMBounds().minY);
	}

	isNormalized = true;
}

Bounds OSMap::getBounds()
{
	return bounds;
}

BoundsUTM OSMap::getUTMBounds()
{
	return utmBounds;
}
