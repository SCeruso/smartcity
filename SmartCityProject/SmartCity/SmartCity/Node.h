#pragma once
#include <cmath>
#include <GeographicLib/Geodesic.hpp>
#include <iostream>
#include <GeographicLib/Constants.hpp>
#include <GeographicLib\GeoCoords.hpp>

using namespace std;
using namespace GeographicLib;
class Node
{
private:
	unsigned long long id;
	double lon;
	double lat;
	double coordX;
	double coordY;
public:
	Node(unsigned long long, double, double);
	~Node();

	double getLat();
	double getLon();
	unsigned long long getId();
	
	double getCoordX();
	double getCoordY();
	
	static double calculateDistance(Node&, Node&);

	void setCoordX(double x);
	void setCoordY(double y);
};

