#pragma once
#include "Highway.h"
#include <vector>
#include<unordered_map>

using namespace std;

typedef struct {
	double minLat;
	double minLon;
	double maxLat;
	double maxLon;
} Bounds;

typedef struct {
	double minX;
	double minY;
	double maxX;
	double maxY;
} BoundsUTM;

class OSMap
{
private:
	unordered_map<unsigned long long, vector<Highway*>> graph;
	unordered_map<unsigned long long, Node*> nodes;
	Bounds bounds;
	BoundsUTM utmBounds;
	bool isNormalized;
public:
	OSMap();
	~OSMap();
	Node* addNode(unsigned long long, double, double);
	Node* getNode(unsigned long long);
	bool hasNode(unsigned long long);
	void addHighway(Node*, string, Node*, int);
	void addHighway(Node*, string, Node*);
	void setBounds(double, double, double, double);
	void setUTMBounds(double, double, double, double);
	void normalize();
	Bounds getBounds();
	BoundsUTM getUTMBounds();
};

