#include "OSMParser.h"

OSMParser::OSMParser(string filename) :map(NULL), filename(filename)
{
}

OSMap * OSMParser::parse()
{
	string path = "../maps/" + filename;
	string aux;
	string highwayString = "highway";
	pugi::xml_document doc;
	pugi::xml_node mapRoot;
	pugi::xml_parse_result result = doc.load_file(path.c_str());
	pugi::xml_node wayNode;
	pugi::xml_node boundsNode;
	mapRoot = doc.child("osm");
	GeoCoords *geocordsMin;
	GeoCoords *geocordsMax;

	std::cout << "Load result: " << result.description() << std::endl;

	if (map != NULL)
		delete map;

	map = new OSMap();

	boundsNode = mapRoot.child("bounds");

	map->setBounds(stod(boundsNode.attribute("minlat").value()), stod(boundsNode.attribute("minlon").value()), stod(boundsNode.attribute("maxlat").value()), stod(boundsNode.attribute("maxlon").value()));

	geocordsMin = new GeoCoords(map->getBounds().minLat, map->getBounds().minLon, UTMUPS::zonespec::UTM);
	geocordsMax = new GeoCoords(map->getBounds().maxLat, map->getBounds().maxLon, UTMUPS::zonespec::UTM);

	map->setUTMBounds(geocordsMin->Easting(), geocordsMin->Northing(), geocordsMax->Easting(), geocordsMax->Northing());

	for (wayNode = mapRoot.child("way"); wayNode; wayNode = wayNode.next_sibling("way")) {
		if (wayNode.find_child_by_attribute("k", "highway") != 0)
			parseHighWay(&wayNode, &mapRoot);
	}

	delete geocordsMin;
	delete geocordsMax;

	return map;
}

OSMap * OSMParser::getMap()
{
	return map;
}

OSMParser::~OSMParser()
{
}

void OSMParser::parseHighWay(pugi::xml_node *wayNode, pugi::xml_node *mapRoot)
{
	pugi::xml_node nodeNode;
	pugi::xml_node ndNode;
	pugi::xml_node tagNode;
	string idString;
	string type;
	string highWayString = "highway";
	string onewayString = "oneway";
	string maxspeedString = "maxspeed";
	string highWayValue;
	string onewayValue;
	string maxspeedValue;
	unsigned long long id;
	int maxSpeed = -1;
	int way = 0;
	vector<unsigned long long> nodesInWay;

	
	
	for (ndNode = wayNode->child("nd"); ndNode; ndNode = ndNode.next_sibling("nd")) {
		idString = ndNode.attribute("ref").value();
		id = std::stoull(idString);
		if (!map->hasNode(id)) {
			nodeNode = mapRoot->find_child_by_attribute("id", idString.c_str());
			map->addNode(id, std::stod(nodeNode.attribute("lat").value()), std::stod(nodeNode.attribute("lon").value()));
		}
		nodesInWay.push_back(id);
	
	}
	for (tagNode = wayNode->child("tag"); tagNode; tagNode = tagNode.next_sibling("tag")) {
		if (highWayString.compare(tagNode.attribute("k").value()) == 0) {
			highWayValue = tagNode.attribute("v").value();
		}
		else if (onewayString.compare(tagNode.attribute("k").value()) == 0) {
			onewayValue = tagNode.attribute("v").value();
			if (onewayValue.compare("yes") == 0)
				way = 1;
			else if (onewayValue.compare("-1") == 0)
				way = -1;
		}
		else if (maxspeedString.compare(tagNode.attribute("k").value()) == 0) {
			maxspeedValue = tagNode.attribute("v").value();
			maxSpeed = stoi(maxspeedValue);
		}
	}
	createHighWay(nodesInWay, highWayValue, way, maxSpeed);

}

void OSMParser::createHighWay(vector<unsigned long long>& nodes, string type, int way, int maxspeed)
{
	for (int i = 1; i < nodes.size(); i++) {
		if (way == 0) {
			map->addHighway(map->getNode(nodes[i - 1]), type, map->getNode(nodes[i]));
			map->addHighway(map->getNode(nodes[i]), type, map->getNode(nodes[i - 1]));
		}

		else if (way == 1) {
			map->addHighway(map->getNode(nodes[i - 1]), type, map->getNode(nodes[i]));
		}
		else if (way == -1) {
			map->addHighway(map->getNode(nodes[i]), type, map->getNode(nodes[i - 1]));
		}
	}
}
