#include "OSMParser.h"


using namespace std;

int main(void) {
	OSMap* map = NULL;
	OSMParser parser("mapaLaLaguna.osm");
	
	map = parser.parse();
	map->normalize();
	if (map != NULL)
		delete map;

	system("pause");
}