#include "Node.h"


Node::Node(unsigned long long id, double lat, double lon): id(id), lat(lat), lon(lon)
{
	GeoCoords geocords(lat, lon, UTMUPS::zonespec::UTM);
   
	setCoordX(geocords.Easting());
	setCoordY(geocords.Northing());
}


Node::~Node()
{
}

double Node::getLat()
{
	return lat;
}

double Node::getLon()
{
	return lon;
}

unsigned long long Node::getId()
{
	return id;
}

double Node::getCoordX() {
	return coordX;
}

double Node::getCoordY() {
	return coordY;
}

void Node::setCoordX(double x)
{
	coordX = x;
}

void Node::setCoordY(double y)
{
	coordY = y;
}


double Node::calculateDistance(Node &n1, Node &n2)
{
	Geodesic geod(Constants::WGS84_a(), Constants::WGS84_f());

	// Distance from JFK to LHR
	double
		lat1 = n1.getLat(), lon1 = n1.getLon(), // JFK Airport
		lat2 = n2.getLat(), lon2 = n2.getLon();  // LHR Airport
	double s12;
	
	 geod.Inverse(lat1, lon1, lat2, lon2, s12);

	 return s12;
}
