#include "Highway.h"



Highway::Highway(Node *origin, double distance, string type, Node *destiny):origin(origin), distance(distance), maxSpeed(-1), destiny(destiny), type(getType(type))
{
	//this->type = getType(type);
}

Highway::Highway(Node *origin, double distance, int maxspeed, string type, Node *destiny): origin(origin), distance(distance), maxSpeed(maxspeed), destiny(destiny), type(getType(type))
{
	//this->type = getType(type);
}

Highway::~Highway()
{
}

bool Highway::hasSpeedLimit()
{
	return maxSpeed > 0? true : false;
}

HighWayType Highway::getType(string type)
{
	if (type.compare("motorway") == 0)
		return MOTORWAY;
	if (type.compare("primary") == 0)
		return PRIMARY;
	if (type.compare("secondary") == 0)
		return SECONDARY;
	if (type.compare("tertiary") == 0)
		return TERTIARY;
	if (type.compare("residential") == 0)
		return RESIDENTIAL;
	if (type.compare("pedestrian") == 0)
		return PEDESTRIAN;
	if (type.compare("service") == 0)
		return SERVICE;
	if (type.compare("footway") == 0)
		return FOOTWAY;
	if (type.compare("secondary_link") == 0)
		return SECONDARY_LINK;
	if (type.compare("steps") == 0)
		return STEPS;
	else
		throw 4;
}
